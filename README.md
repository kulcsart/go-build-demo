# README #

This is demo project that demonstates how to create Go build chain using 
Jenkins pipelines.

### What is this repository for? ###

* Contains a demo project with Jenkinsfile a some go code to compile
* v 0.0.1

### How do I get set up? ###

* Clone the repo with jenkins
* Follow the instructions

### Contribution guidelines ###

* Feel free to contribute just make sure that the code actually works
